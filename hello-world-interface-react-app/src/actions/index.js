export const SET_BOTTOM_NAV_OPTION = 'SET_BOTTOM_NAV_OPTION'
// Esto se hace simplemente para no trabajar con strings sino con constantes y que al momento de hacer
// los reducers no haya conflicto con strings que coincidan

export const setBottomNavOption = payload => ({ type: SET_BOTTOM_NAV_OPTION , payload});
  /*Creo la accion que voy a disparar, ésta va a recibir como parametro un payload (valor),
  indico el type (descripcion) de la accion y el 2do parametro es el valor que está recibiendo
  como payload, que luego en el archivo BottomNavigation.js, en el dispatch, le indico que ese valor que recibe como parametro
  es el estado o elvalue del BottomNavigation*/