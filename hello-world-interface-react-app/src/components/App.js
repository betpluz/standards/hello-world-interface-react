import React, { Component } from 'react';
import '../css/App.css';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import Desktop from './desktop/Desktop';
import Mobile from './mobile/Mobile';


class App extends Component {
  render() {
  	if (isWidthUp('sm', this.props.width)) { // Acá evaluo si, el tamaño de pantalla es mayor a 'sm'(pequeño: 600px o mayor)
  											 // Devuelvo solo el componente Desktop.
      return (
      	<div className="App">
      		<Desktop />
      	</div>
      );
    }
    										// Si no... (Estoy en dispositivo movil) Devuelvo el componente en su version Movil.
    return (
      <div className="App">
        <Mobile />
      </div>
    );
  }
}

export default withWidth()(App);
