import React, { Component } from 'react';
import logo from '../logo.svg';
import '../css/Home.css';

class Home extends Component {
  render() {
    return (
      <div className="home">
        <header className="home-header">
          <img src={logo} className="home-logo" alt="logo" />
          <h1>Hello World, BetPluz!</h1>
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
        </header>
      </div>
    );
  }
}

export default Home;
