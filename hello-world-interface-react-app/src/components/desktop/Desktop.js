import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import MenuNav from './desktop-components/MenuNav';
import About from './desktop-components/About';
import Contact from './desktop-components/Contact';
import Home from '../Home';



class Desktop extends Component {
  render() {
    return (
      <BrowserRouter>
      	<div>
      	  <MenuNav />
      	  <Route exact path="/" component={Home}/>
      	  <Route path="/about" component={About}/>
      	  <Route path="/contact" component={Contact}/>
      	</div>
      </BrowserRouter>
    );
  }
}

export default Desktop;
