//Dependencies
import React from 'react';
import 'materialize-css/dist/css/materialize.min.css';
//Components
import GridAbout from './grid-components/GridAbout';
//CSS
import '../../../css/About.css';

const About = () => {
	return (
		<div className="cont-position pad">
			<div className="center"><h3>About</h3></div>
			<GridAbout />
		</div>
	);
}
export default About;