//Dependencies
import React from 'react';
import 'materialize-css/dist/css/materialize.min.css';
//Components
import GridContact from './grid-components/GridContact';
//CSS
import '../../../css/Contact.css';

const Contact = () => {
	return (
		<div className="cont-position pad">
			<div className="center"><h3>Contact</h3></div>
			<GridContact />
		</div>
	);
}
export default Contact;