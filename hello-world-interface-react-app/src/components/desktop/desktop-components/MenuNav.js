//Dependencies
import React from 'react';
import { Link } from 'react-router-dom';
//CSS
import 'materialize-css/dist/css/materialize.min.css';
import '../../../css/Menu.css'

const MenuNav = () => {
	return (
		<nav className="nav-wrapper menu">
			<div className="container">
				<li className="left brand-logo">BetPluz</li>
				<ul className="right">
					<li><Link to="/">Home</Link></li>
					<li><Link to="/about">About Us</Link></li>
					<li><Link to="/contact">Contact</Link></li>
				</ul>
			</div>
		</nav>
	);
}
export default MenuNav;