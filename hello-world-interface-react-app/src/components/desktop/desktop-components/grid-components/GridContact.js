import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import 'materialize-css/dist/css/materialize.min.css';
import ColumnLeft from '../../../mobile/mobile-components/about/bottom-nav-components/ColumnLeft';
import ColumnCenter from '../../../mobile/mobile-components/about/bottom-nav-components/ColumnCenter';
import ColumnRight from '../../../mobile/mobile-components/contact/bottom-nav-components/ColumnRight';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 140,
    width: 100,
  },
  control: {
    padding: theme.spacing.unit * 2,
  },
});

class GridContact extends React.Component {
  render() {
    const { classes } = this.props;

    return (
      <Grid container direction="row" justify="center" alignItems="center" className={classes.root} spacing={16}>
        <Grid item md={4} xs={8}>
          <ColumnLeft />
        </Grid>
        <Grid item md={4} xs={8}>
          <ColumnCenter />
        </Grid>
        <Grid item md={4} xs={8}>
          <ColumnRight />
        </Grid>
      </Grid>
    );
  }
}

GridContact.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(GridContact);