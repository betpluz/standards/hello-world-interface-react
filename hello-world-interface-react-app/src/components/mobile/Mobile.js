// Dependencies
import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
//Components
import Navbar from './mobile-components/Navbar';
import Home from '../Home';
import About from './mobile-components/about/About';
import Contact from './mobile-components/contact/Contact';

class Mobile extends Component {
  render() {
    return (
      <BrowserRouter>
      	<div>
      	  <Navbar />
      	  <Route exact path="/" component={Home}/>
      	  <Route path="/about" component={About}/>
          <Route path="/contact" component={Contact}/>
      	</div>
      </BrowserRouter>
    );
  }
}

export default Mobile;
