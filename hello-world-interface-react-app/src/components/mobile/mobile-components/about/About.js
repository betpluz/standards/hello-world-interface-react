//Dependencies
import React from 'react';
//Components
import BottomNavigation from './BottomNavigation';
//CSS
import '../../../../css/About.css';

const About = () => {
	return (
		<div>
			<div className="title-position"><h3 className="center">About us</h3></div>
			<BottomNavigation />
		</div>
	);
}
export default About;