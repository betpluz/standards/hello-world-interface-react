import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import ColumnLeft from './bottom-nav-components/ColumnLeft'
import ColumnCenter from './bottom-nav-components/ColumnCenter'
import ColumnRight from './bottom-nav-components/ColumnRight'

const styles = {
  root: {
    width: '100%',
  },
  bottomContainer: {
    width: '100%',
    position: 'fixed',
    bottom: 0,
  },
  pad: {
    padding: '0px 20px',
  },
};

class SimpleBottomNavigation extends React.Component {
  state = {
    value: 'Left',
  };

  handleChange = (event, value) => {
    //console.log(value);
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
        <div>
          {value === 'Left' && <div className={classes.pad}> <ColumnLeft/> </div>}
          {value === 'Center'&& <div className={classes.pad}> <ColumnCenter/> </div>}
          {value === 'Right'&& <div className={classes.pad}> <ColumnRight/> </div>}
          <div className={classes.bottomContainer}>
            <BottomNavigation
              value={value}
              onChange={this.handleChange}
              showLabels
              className={classes.root}
            >
              <BottomNavigationAction label="Left" value="Left" icon={<RestoreIcon />} />
              <BottomNavigationAction label="Center" value="Center" icon={<FavoriteIcon />} />
              <BottomNavigationAction label="Right" value="Right" icon={<LocationOnIcon />} />
            </BottomNavigation>
          </div>
        </div>
    );
  }
}

SimpleBottomNavigation.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleBottomNavigation);
