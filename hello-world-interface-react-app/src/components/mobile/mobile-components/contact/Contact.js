//Dependencies
import React from 'react';
//Components
import BottomNavigation from './BottomNavigation';
//CSS
import '../../../../css/Contact.css';

const Contact = () => {
	return (
		<div>
			<div className="title-position"><h3 className="center">Contact</h3></div>
			<BottomNavigation />
		</div>
	);
}
export default Contact;