//Dependencies
import React from 'react';
import 'materialize-css/dist/css/materialize.min.css';
import '../../../../../css/Contact.css';


const ColumnLeft = () => {
	return (
		<div className="cont-col">
			<div className="container center"><h5>ColumnLeft</h5></div>
			<div className="card teal lighten-1">
				<div className="card-content white-text">
					<span className="card-title">Card Title</span>
					<p>I am a very simple card. I am good at containing small bits of information.
					I am convenient because I require little markup to use effectively.</p>
				</div>
			</div>
		</div>

	);
}
export default ColumnLeft;